/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NEET.test;

public class TestResultBean {
    
    private int testId;
    private int totalPhysics,totalChemistry,totalBiology;
    private int obtainedPhysics,obtainedChemistry,obtainedBiology;

    public TestResultBean() {
    }

    public TestResultBean(int testId,int totalPhysics, int totalChemistry, int totalBiology, int obtainedPhysics, int obtainedChemistry, int obtainedBiology) {
        this.testId = testId;
        this.totalPhysics = totalPhysics;
        this.totalChemistry = totalChemistry;
        this.totalBiology = totalBiology;
        this.obtainedPhysics = obtainedPhysics;
        this.obtainedChemistry = obtainedChemistry;
        this.obtainedBiology = obtainedBiology;
    }

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }
    
    public int getObtainedBiology() {
        return obtainedBiology;
    }

    public void setObtainedBiology(int obtainedBiology) {
        this.obtainedBiology = obtainedBiology;
    }

    public int getObtainedChemistry() {
        return obtainedChemistry;
    }

    public void setObtainedChemistry(int obtainedChemistry) {
        this.obtainedChemistry = obtainedChemistry;
    }

    public int getObtainedPhysics() {
        return obtainedPhysics;
    }

    public void setObtainedPhysics(int obtainedPhysics) {
        this.obtainedPhysics = obtainedPhysics;
    }

    public int getTotalBiology() {
        return totalBiology;
    }

    public void setTotalBiology(int totalBiology) {
        this.totalBiology = totalBiology;
    }

    public int getTotalChemistry() {
        return totalChemistry;
    }

    public void setTotalChemistry(int totalChemistry) {
        this.totalChemistry = totalChemistry;
    }

    public int getTotalPhysics() {
        return totalPhysics;
    }

    public void setTotalPhysics(int totalPhysics) {
        this.totalPhysics = totalPhysics;
    }
}