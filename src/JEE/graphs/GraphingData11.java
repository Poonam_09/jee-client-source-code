package JEE.graphs;

import java.awt.*;
import java.awt.geom.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;

public class GraphingData11 extends JPanel 
{
    ArrayList<Integer> data;
    String groupName = "";
    int totalQuestions = 50;   
    int rollNo;
    public GraphingData11(int rollNo) {
        super();
        this.rollNo=rollNo;
        this.data=new JEE.test.DBConnection().getSubjectResultData(2,rollNo);
    }
    
public GraphingData11(ArrayList<Integer> data,String groupName,int totalQuestions,int rollNo) {
        super();
        this.rollNo=rollNo;
        this.data=data;
        this.groupName=groupName;
        this.totalQuestions=totalQuestions;
    }
    	final int PAD = 30;
	int w,h;			
	double xInc,scale;
        @Override
    	protected void paintComponent(Graphics g) 
	{
        	super.paintComponent(g);
        	Graphics2D g2 = (Graphics2D)g;
        	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);
        	w = getWidth();
        	h = getHeight();
                g2.setPaint(Color.BLACK);
        	g2.draw(new Line2D.Double(PAD, PAD, PAD, h-PAD));
 	       	g2.draw(new Line2D.Double(PAD, h-PAD, w-PAD, h-PAD));
		xInc = (double)(w - 2*PAD)/(data.size()-1);
        	scale = (double)(h - 2*PAD)/totalQuestions;
		g2.setPaint(Color.BLACK);
		for(int i=5;i<=totalQuestions;i=i+5)
		{
			double x=PAD;
			double y = h - PAD - scale*i;
			g.drawString(Integer.toString(i), (int)x-20, (int)y);
			g2.fill(new Ellipse2D.Double(x-2, y-2, 4, 4));
		}
		for(int i = 0; i < data.size(); i++) 
		{
            		double x = PAD + i*xInc;
            		double y = h-PAD;
	    		g.drawString(Integer.toString(i+1), (int)x, (int)y+20);
			g2.fill(new Ellipse2D.Double(x-2, y-2, 4, 4));	
        	}
        	g2.setPaint(Color.red);
		double xp=0,yp=0;	
        	for(int i = 0; i < data.size(); i++) 
		{
            		double x = PAD + i*xInc;
            		double y = h - PAD - scale*data.get(i);
	    		if(i!=0)
	    		{
            			g2.draw(new Line2D.Double(xp, yp, x, y));
	    		}
			xp=x;
			yp=y;		
        	}
    	}

        public static void main(String[] args) throws IOException 
	{
//        	JFrame f = new JFrame();
//        	f.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
//                GraphingData11 graphingData11=new GraphingData11();
//                graphingData11.setBackground(Color.WHITE);
//        	f.add(graphingData11);
//                f.setTitle(graphingData11.groupName);
//        	f.setSize(600,600);
//        	f.setLocation(250,100);
//        	f.setVisible(true);
//                File imagePath = new File("src/ui/images/c.gif");
//                Image image=ImageIO.read(imagePath);
//                f.setIconImage(image);
    	}
}