/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;


import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

/**
 *
 * @author Aniket
 */
public class FileChooser {
    
    
    
    public String getFolderLocation(OSKSetting oskSetting,String path) {
        String returnString = null;
        try {
            JFileChooser chooser = new JFileChooser(); 
            String choosertitle = "";
            //new java.io.File(System.getProperty("user.home") + File.separator + "Documents")
            chooser.setCurrentDirectory(new File(path));
            chooser.setDialogTitle(choosertitle);
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            //
            // disable the "All files" option.
            //
            chooser.setAcceptAllFileFilterUsed(false);
            //    
            if (chooser.showOpenDialog(oskSetting) == JFileChooser.APPROVE_OPTION) { 
                returnString = chooser.getSelectedFile().toString();
            } else {
                returnString = null;
            }
        } catch (Exception ex) {
            returnString = null;
        }
        return returnString;
    }
    
    
}
