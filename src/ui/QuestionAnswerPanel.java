/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * QuestionPanel.java
 *
 * Created on Nov 5, 2012, 2:28:02 PM
 */
package ui;

import JEE.test.DBConnection;
import com.bean.QuestionBean;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import org.scilab.forge.jlatexmath.*;


/**
 *
 * @author Administrator
 */
public class QuestionAnswerPanel extends javax.swing.JPanel {

    /** Creates new form QuestionPanel */
    String start,end;
    QuestionBean currentQuestion;
    QuestionBean hint;
    String numericalAnswer;
    String qImagePath = "", oImagePath = "";
    public QuestionAnswerPanel()
    {
        initComponents();  
    }
    
    public QuestionAnswerPanel(QuestionBean questionBean, int index,String numericalAnswer ) {
        initComponents(); 
        
        start = "\\begin{array}{l}";
	end = "\\end{array}";
        
        lblHintm.setVisible(false);
        rdoOptionA.setEnabled(false);
        rdoOptionB.setEnabled(false);
        rdoOptionC.setEnabled(false);
        rdoOptionD.setEnabled(false);
        lblHint.setVisible(false);
        this.numericalAnswer=numericalAnswer;
        setQuestionOnPanel(questionBean, index,numericalAnswer.trim());
//        rdoOptionA.setActionCommand("A");
//        rdoOptionB.setActionCommand("B");
//        rdoOptionC.setActionCommand("C");
//        rdoOptionD.setActionCommand("D");        
       
//        buttonGroup1.add(rdoOptionA);        
//        buttonGroup1.add(rdoOptionB);
//        buttonGroup1.add(rdoOptionC);
//        buttonGroup1.add(rdoOptionD);
    }
    
    public void showOptions()
    {
                lblA.setVisible(true);
                lblOptionA.setVisible(true);
                lblB.setVisible(true);
                lblOptionB.setVisible(true);
                lblC.setVisible(true);
                lblOptionC.setVisible(true);
                lblD.setVisible(true);
                lblOptionD.setVisible(true);
                lblOptionAsImage.setVisible(false);                
    }
    public void showHint()
    {
        
    }
    public void hideOptions()
    {
                lblA.setVisible(false);
                lblOptionA.setVisible(false);
                lblB.setVisible(false);
                lblOptionB.setVisible(false);
                lblC.setVisible(false);
                lblOptionC.setVisible(false);
                lblD.setVisible(false);
                lblOptionD.setVisible(false);
                lblOptionAsImage.setVisible(true);                
    }
    
    public void setLableText(JLabel l,String str)
    {
        try
        {
            if(str.equals(""))
            {
                
            }
            else
            {
                l.setText("");
                TeXFormula formula;
                TeXIcon icon;
                BufferedImage image;
                Graphics2D g2;                
                JLabel jl;
                str=start+str+end;
                formula = new TeXFormula(str);
                icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
                icon.setInsets(new Insets(0,0,0,0));
                image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                g2 = image.createGraphics();
                g2.setColor(Color.white);
                g2.fillRect(0,0,icon.getIconWidth(),icon.getIconHeight());
                jl = new JLabel();
                jl.setForeground(new Color(0, 0, 0));
                icon.paintIcon(jl, g2, 0, 0);
                l.setIcon(icon);   
            }
        }
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null,e.getMessage()); 
        }
    }
    
    public void showQuestionImage()
    {         
                lblQuestionAsImage.setVisible(true);                
    }
    public void hideQuestionImage()
    {                
                lblQuestionAsImage.setVisible(false);                
    }
    
    public void showHintImage()
    {         
                lblHintImage.setVisible(true);                
    }
    public void hideHintImage()
    {                
                lblHintImage.setVisible(false);                
    }
    
    public void loadImage(String path,JLabel lbl)
    {   
        System.out.println("Image path"+path);
        String[] ImagePath = path.split("/");
        System.out.println("Image path 1="+ImagePath[0]);
        System.out.println("Image path 2="+ImagePath[1]);
           
        String CurrentPatternName=new DBConnection().getCurrentPatternName();
        System.out.println("Image path 2 ="+CurrentPatternName+"/"+ImagePath[1]);
        String path1=CurrentPatternName+"/"+ImagePath[1];
        BufferedImage image;
        try{            
                File file=new File(path1);
                image=ImageIO.read(file);
                ImageIcon icon;
                float width=image.getWidth();
                float height=image.getHeight();
                //Determine how the image has to be scaled if it is large:
                Image thumb = image.getScaledInstance((int)width,(int)height, Image.SCALE_AREA_AVERAGING);
                icon=new ImageIcon(thumb);
                
                lbl.setIcon(icon);
                lbl.setText("");               
            }        
            catch(Exception e)
            {
                System.out.println(e);
            }
        }
    public String removeMbox(String str) {
//        Quest.stringcheck.NewClass ob = new Quest.stringcheck.NewClass();
//        str=ob.Managequotes(str);Managequotes
        
        str = str.replace("\\mbox {", "\\mbox{");
        str = str.replace("\\mbox {", "\\mbox{");
        str = removeDoubleSlash(str);
        str = "  "+str+"  ";
        char [] carray = str.toCharArray();
        char [] tarray = new char[str.length()+7] ;
        int clen = str.length();
        int k=0,j=0;int flag1=0;
        for(int i=0;i<clen;)
        {
            if(carray[i]=='\\'&&carray[i+1]=='m'&&carray[i+2]=='b'&&carray[i+3]=='o'&&carray[i+4]=='x'&&carray[i+5]=='{')
            {
                if(flag1==1&&carray[i+6]!=' ')
                {
                    tarray[j]=' ';j++;
                }
                i+=6;
                k=newSingleclosingBrackPosition(str, i-6);
                for(;i<k&&i<clen;)
                {
                    tarray[j]=carray[i];i++;j++;
                }
                i++;
                flag1=1;
            }
            else
            {
                tarray[j]=carray[i];i++;j++;
            }
        }
        String s1=new String(tarray);
        s1=s1.trim();
        
        String numA=s1.replace("$", "");
        numA=s1.replace("$\\", "");
        numA=s1.replace("$", "");
        String numA1=numA.replace("\\", "");
        String numA2=numA1.replace("\\minus", "-");
        
        return  numA2;
    }
     public int newSingleclosingBrackPosition(String str,int position) {
       char [] carray = str.toCharArray();
       int clen=carray.length;
       int count=0;
       int i=position;
       for(;carray[i]!='{';)
       {
           i++;
       } 
       for(;i<clen;i++)
       {
           if(carray[i]=='{'&&carray[i-1]!='\\')
           {
               count+=1;
           }
           if(carray[i]=='}'&&carray[i-1]!='\\')
           {
               count-=1;
           }
           if(count==0)
           {
               return i;
           }
       }
       return 0;
    }
     public String removeDoubleSlash(String str) {
        char [] carray = str.toCharArray();
        char [] tarray = new char[str.length()+7] ;
        int clen = str.length();
        int j=0;
        for(int i=0;i<clen;)
        {
            
            if(carray[i]=='\\'&&carray[i+1]=='\\')
            {
                int beginIndex=str.indexOf("\\begin{", i);
                int endIndex=str.indexOf("\\end{", i);
                
                if(endIndex==-1)
                {
                    i+=2;                                    
                }
                else
                {
                    if(beginIndex==-1||beginIndex>endIndex)
                    {
                        tarray[j]=carray[i];i++;j++;
                        tarray[j]=carray[i];i++;j++;                        
                    }
                    else
                    {
                        i+=2;
                    }
                }
            }
            else
            {
                tarray[j]=carray[i];
                i++;
                j++;
            }
        }
        
        String s1=new String(tarray);
        s1=s1.trim();
        
        return s1;
    }
     
      public void setQuestionOnPanel(QuestionBean question,int index,String numericalAnswer)
    { 
            qImagePath = question.getQuestionImagePath();
            oImagePath = question.getOptionImagePath(); 
            currentQuestion=question;
            hint=question;
            lblHintm.setVisible(false);
            if( question.getHint().equals("\\mbox{null}") ||question.getHint().equals("\\mbox{ }") ||question.getHint().equals("\\mbox{  }") ||question.getHint().equals("\\mbox{   }") ||question.getHint().equals("\\mbox{\\mbox{}}") ||question.getHint().equals("\\mbox{") ||question.getHint().equals("\\mbox{    }") ||question.getHint().equals("") ||question.getHint() == null||question.getHint().equals("\\mbox{}"))
                {
                    lblHintm.setVisible(false);
                    lblHint.setVisible(false);
                }
                else
                {
                    lblHint.setVisible(true);
                    lblHintm.setVisible(true);
                    setLableText(lblHintm,question.getHint());
                }
           
            setLableText(lblQuestion,question.getQuestion());
            if(!question.isIsQuestionAsImage())
            {
                hideQuestionImage();
            }
            else
            {
                showQuestionImage();                
                loadImage(question.getQuestionImagePath(), lblQuestionAsImage);                
            }
            if(!question.isIsHintAsImage())
            {
                hideHintImage();
            }
            else
            {
                showHintImage();                
                loadImage(question.getHintImagePath(), lblHintImage);                
            }
            
            if(question.getType()==0||question.getType()==1)
        {
                txtNumAns.setVisible(false);
                lblCorrectNumericalAnswer.setVisible(false); 
                lblUserNumericalAnswer.setVisible(false);
                txtNumericalAnswer.setVisible(false);
               
                lblQuestionNo6.setVisible(true);
                lblOptionAsImage.setVisible(true);
                lblCorrect.setVisible(true);
                lblCorrectAnswer.setVisible(true);
                lblQuestionNo5.setVisible(true);
                rdoOptionA.setVisible(true);
                rdoOptionB.setVisible(true);
                rdoOptionC.setVisible(true);
                rdoOptionD.setVisible(true);
            
            if(!question.isIsOptionAsImage())
            {
                showOptions();                
                setLableText(lblOptionA,question.getOptionA());            
                setLableText(lblOptionB,question.getOptionB());
                setLableText(lblOptionC,question.getOptionC());
                setLableText(lblOptionD,question.getOptionD());
            }
            else
            {
                hideOptions();   
                loadImage(question.getOptionImagePath(), lblOptionAsImage);
            }            
            
            String userAnswer=question.getUserAnswer();
            if(question.getUserAnswer().equalsIgnoreCase("UnAttempted"))// userAnswer.equals("UnAttempted")
            {    
                System.out.println("******UnAttempted");        
                lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/unAttempted.png")));
                lblCorrectAnswer.setText(currentQuestion.getAnswer());
            }
            else
            {
                       if(userAnswer.equals("A")) 
                        {
                            rdoOptionA.setSelected(true);
                        }
                        else if(userAnswer.equals("B"))
                        {
                            rdoOptionB.setSelected(true);
                        }
                        else if(userAnswer.equals("C"))
                        {
                            rdoOptionC.setSelected(true);
                        }
                        if(userAnswer.equals("D"))
                        {
                            rdoOptionD.setSelected(true);
                        }     
                
                if(userAnswer.equals(question.getAnswer()))  //  userAnswer.equals(currentQuestion.getAnswer())
                {
                       
                     
                  lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/correct.png")));
//                  lblCorrect.setVisible(false);
                  lblCorrectAnswer.setText(currentQuestion.getAnswer());
//                  lblCorrectAnswer.setVisible(false);
                    System.out.println("******Correct Image");
                    
                }
                else
                { 
                    System.out.println("******InCorrect Image");
                    lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wrong.png")));
                    lblCorrectAnswer.setText(currentQuestion.getAnswer());
                }
            } 
        }        
        else if(question.getType()==2)
        { 
               lblUserNumericalAnswer.setVisible(true);
               txtNumericalAnswer.setVisible(true);
               txtNumAns.setVisible(true);
               lblCorrectNumericalAnswer.setVisible(true);
               
                hideOptions();
                
                lblQuestionNo6.setVisible(false);
                lblOptionAsImage.setVisible(false);
                lblCorrect.setVisible(false);
                lblCorrectAnswer.setVisible(false);
                lblQuestionNo5.setVisible(false);
                rdoOptionA.setVisible(false);
                rdoOptionB.setVisible(false);
                rdoOptionC.setVisible(false);
                rdoOptionD.setVisible(false);
                
                txtNumericalAnswer.setText(numericalAnswer.trim());
//                String NumericalAnswer=question.getNumericalAnswer();
                if(numericalAnswer.trim().equalsIgnoreCase("UnAttempted"))// userAnswer.equals("UnAttempted")
                {   
                      System.out.println("******UnAttempted");        
                      lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/unAttempted.png")));
                     // txtNumAns.setText(currentQuestion.getNumericalAnswer()+currentQuestion.getUnit());
                      setLableText(txtNumAns,currentQuestion.getNumericalAnswer().trim()+currentQuestion.getUnit().trim());
                    System.out.println("currentQuestion.getNumericalAnswer().trim()---"+currentQuestion.getNumericalAnswer().trim());
                    System.out.println("currentQuestion.getUnit().trim()----"+currentQuestion.getUnit().trim());
                }   
                else
                {
                                         
                  //  String RightNumAns=removeMbox(currentQuestion.getNumericalAnswer().trim()+currentQuestion.getUnit()).trim();
                  
                       String RightNum=removeMbox(currentQuestion.getNumericalAnswer().trim()+currentQuestion.getUnit().trim());
                       String RightNumAns1=removeMbox(currentQuestion.getNumericalAnswer().trim());
//                                        float num= Float.parseFloat(RightNumAns);
//                                        String RightNumAns1=String.valueOf(Math.round(num));
//                                        System.out.println("RightNumAns--->"+RightNumAns);
//                                        System.out.println("RightNumAns1--->"+RightNumAns1);
                // for database answer                        
                  String temp4="";
                  String str1 =  numericalAnswer.replace("\\", "&");
                 char[] temp3=str1.toCharArray();
                 for(int i=0;i<temp3.length;i++){
                    
                 if(temp3[i]==' ')
                 {
                      System.out.println("temp4*************================="+temp4);      
                      
                 }else if(temp3[i]=='&'){
                     break;
                 }
                 else{
                   
                     temp4=temp4+temp3[i];
                    
                }
                    
                }
                  // for user entered ans                      
                String temp2="";
                String str = numericalAnswer.replace("\\", "&");
                char[] temp1=str.toCharArray();
                for(int i=0;i<temp1.length;i++){
                    
                 if(temp1[i]==' '){
                      System.out.println("temp2*************==========================================="+temp2);

                     
                 }else if(temp1[i]=='&'){
                     break;
                 }
                 else{
                   
                     temp2=temp2+temp1[i];
                    
                }
                    
                }
                     String temp5="";
                 char[] temp6=str.toCharArray();
                  System.out.println("str-------------------"+str);
                 for(int i=0;i<temp6.length;i++){
                      if(temp6[i]==' ')
                       {
                          System.out.println("temp5*************================="+temp5);      
                      
                        }else if(Character.isDigit(temp6[i]))
                        {
                            
                             temp5=temp5+temp6[i];
                        }
                        
                        
                    
                }        
                  System.out.println("temp5*************================="+temp5);   
                  System.out.println("temp4*************================="+temp4);
                    System.out.println("RightNumAns*************================="+RightNumAns1);
                    System.out.println("RightNum*************================="+RightNum); 
                    
                      String numAns="";
                    for(int i=0;i<RightNumAns1.toCharArray().length;i++){
                       
                        
                        if(RightNumAns1.toCharArray()[i]=='.'){
                            
                           break; 
                            
                        }else{
                           numAns=numAns+ RightNumAns1.toCharArray()[i];
                            
                        }
                        
                    }
                    
                     String RightNumAns=numAns;
                    if (temp5.trim().equals(RightNumAns.trim()) || temp4.trim().equals(RightNum.trim()))// || temp4.trim().equals(RightNum.trim())
                                             
                                        {
                                                
                  
                       System.out.println("------------Poonam----------------------");
                       lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/"
                               + "correct.png")));
   
                         setLableText(txtNumAns,currentQuestion.getNumericalAnswer().trim()+currentQuestion.getUnit().trim());
                         System.out.println("******Correct Image");
                         

                     }
                     else
                     { 
                         System.out.println("******InCorrect Image");
                         lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wrong.png")));                      
                          setLableText(txtNumAns,currentQuestion.getNumericalAnswer().trim()+currentQuestion.getUnit().trim());
                     }
                }
                      
        }    
            lblQuestionNo.setText("Q. "+index);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        lblOptionAsImage = new javax.swing.JLabel();
        rdoOptionA = new javax.swing.JRadioButton();
        lblOptionA = new javax.swing.JLabel();
        rdoOptionC = new javax.swing.JRadioButton();
        lblOptionB = new javax.swing.JLabel();
        rdoOptionB = new javax.swing.JRadioButton();
        lblB = new javax.swing.JLabel();
        lblD = new javax.swing.JLabel();
        lblOptionC = new javax.swing.JLabel();
        lblC = new javax.swing.JLabel();
        lblQuestionNo5 = new javax.swing.JLabel();
        lblQuestion = new javax.swing.JLabel();
        rdoOptionD = new javax.swing.JRadioButton();
        lblQuestionNo = new javax.swing.JLabel();
        lblA = new javax.swing.JLabel();
        lblQuestionNo6 = new javax.swing.JLabel();
        lblOptionD = new javax.swing.JLabel();
        lblQuestionAsImage = new javax.swing.JLabel();
        lblHint = new javax.swing.JLabel();
        lblRightWrongSymbol = new javax.swing.JLabel();
        lblCorrect = new javax.swing.JLabel();
        lblCorrectAnswer = new javax.swing.JLabel();
        lblHintm = new javax.swing.JLabel();
        lblHintImage = new javax.swing.JLabel();
        lblUserNumericalAnswer = new javax.swing.JLabel();
        txtNumericalAnswer = new javax.swing.JLabel();
        lblCorrectNumericalAnswer = new javax.swing.JLabel();
        txtNumAns = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setAutoscrolls(true);
        setPreferredSize(new java.awt.Dimension(765, 500));

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setName("jPanel1"); // NOI18N

        lblOptionAsImage.setText("Option Image");
        lblOptionAsImage.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionAsImage.setName("lblOptionAsImage"); // NOI18N
        lblOptionAsImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblOptionAsImageMouseClicked(evt);
            }
        });

        rdoOptionA.setText("A");
        rdoOptionA.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoOptionA.setName("rdoOptionA"); // NOI18N
        rdoOptionA.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionAItemStateChanged(evt);
            }
        });

        lblOptionA.setText("jLabel7");
        lblOptionA.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionA.setName("lblOptionA"); // NOI18N

        rdoOptionC.setText("C");
        rdoOptionC.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoOptionC.setName("rdoOptionC"); // NOI18N
        rdoOptionC.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionCItemStateChanged(evt);
            }
        });

        lblOptionB.setText("jLabel7");
        lblOptionB.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionB.setName("lblOptionB"); // NOI18N

        rdoOptionB.setText("B");
        rdoOptionB.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoOptionB.setName("rdoOptionB"); // NOI18N
        rdoOptionB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionBItemStateChanged(evt);
            }
        });

        lblB.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblB.setText("B :");
        lblB.setName("lblB"); // NOI18N

        lblD.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblD.setText("D :");
        lblD.setName("lblD"); // NOI18N

        lblOptionC.setText("jLabel7");
        lblOptionC.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionC.setName("lblOptionC"); // NOI18N

        lblC.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblC.setText("C :");
        lblC.setName("lblC"); // NOI18N

        lblQuestionNo5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQuestionNo5.setText("User Answer:");
        lblQuestionNo5.setName("lblQuestionNo5"); // NOI18N

        lblQuestion.setText("Question"); // NOI18N
        lblQuestion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQuestion.setName("lblQuestion"); // NOI18N

        rdoOptionD.setText("D");
        rdoOptionD.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoOptionD.setName("rdoOptionD"); // NOI18N
        rdoOptionD.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionDItemStateChanged(evt);
            }
        });

        lblQuestionNo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQuestionNo.setText("Q. 1");
        lblQuestionNo.setName("lblQuestionNo"); // NOI18N

        lblA.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblA.setText("A :");
        lblA.setName("lblA"); // NOI18N

        lblQuestionNo6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQuestionNo6.setText("Options :");
        lblQuestionNo6.setName("lblQuestionNo6"); // NOI18N

        lblOptionD.setText("jLabel7");
        lblOptionD.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionD.setName("lblOptionD"); // NOI18N

        lblQuestionAsImage.setText("Question Image");
        lblQuestionAsImage.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQuestionAsImage.setName("lblQuestionAsImage"); // NOI18N
        lblQuestionAsImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblQuestionAsImageMouseClicked(evt);
            }
        });

        lblHint.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblHint.setText("Hint:");
        lblHint.setName("lblHint"); // NOI18N

        lblRightWrongSymbol.setBackground(new java.awt.Color(255, 255, 255));
        lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/correct.png"))); // NOI18N
        lblRightWrongSymbol.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblRightWrongSymbol.setName("lblRightWrongSymbol"); // NOI18N

        lblCorrect.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblCorrect.setText("Correct Answer:");
        lblCorrect.setName("lblCorrect"); // NOI18N

        lblCorrectAnswer.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblCorrectAnswer.setText("A");
        lblCorrectAnswer.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblCorrectAnswer.setName("lblCorrectAnswer"); // NOI18N

        lblHintm.setForeground(new java.awt.Color(255, 0, 0));
        lblHintm.setText("Hint");
        lblHintm.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblHintm.setName("lblHintm"); // NOI18N

        lblHintImage.setText("HintImage");
        lblHintImage.setName("lblHintImage"); // NOI18N

        lblUserNumericalAnswer.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblUserNumericalAnswer.setText("User Numerical Answer:");
        lblUserNumericalAnswer.setName("lblUserNumericalAnswer"); // NOI18N

        txtNumericalAnswer.setText("jLabel3");
        txtNumericalAnswer.setName("txtNumericalAnswer"); // NOI18N

        lblCorrectNumericalAnswer.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblCorrectNumericalAnswer.setText("Correct Numerical Answer:");
        lblCorrectNumericalAnswer.setName("lblCorrectNumericalAnswer"); // NOI18N

        txtNumAns.setText("jLabel3");
        txtNumAns.setName("txtNumAns"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(179, 179, 179)
                .addComponent(lblHintImage)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCorrect)
                            .addComponent(lblHint))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblHintm, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCorrectAnswer, javax.swing.GroupLayout.Alignment.LEADING)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblQuestionNo)
                        .addGap(28, 28, 28)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblQuestionAsImage)
                            .addComponent(lblQuestion)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblQuestionNo6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblB)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionB))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblA)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionA))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblC)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionC))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblD)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionD))
                            .addComponent(lblOptionAsImage)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblQuestionNo5, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(rdoOptionA)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rdoOptionB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rdoOptionC)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rdoOptionD)
                        .addGap(18, 18, 18)
                        .addComponent(lblRightWrongSymbol))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(lblCorrectNumericalAnswer)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtNumAns, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addComponent(lblUserNumericalAnswer)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(txtNumericalAnswer, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(348, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo)
                    .addComponent(lblQuestion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblQuestionAsImage)
                .addGap(1, 1, 1)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserNumericalAnswer)
                    .addComponent(txtNumericalAnswer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo6)
                    .addComponent(lblA)
                    .addComponent(lblOptionA))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB)
                    .addComponent(lblOptionB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblC)
                    .addComponent(lblOptionC))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblD)
                    .addComponent(lblOptionD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblOptionAsImage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblRightWrongSymbol)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblQuestionNo5)
                        .addComponent(rdoOptionA)
                        .addComponent(rdoOptionB)
                        .addComponent(rdoOptionC)
                        .addComponent(rdoOptionD)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(lblCorrectAnswer))
                    .addComponent(lblCorrect))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCorrectNumericalAnswer)
                    .addComponent(txtNumAns))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblHintm)
                    .addComponent(lblHint))
                .addGap(18, 18, 18)
                .addComponent(lblHintImage)
                .addContainerGap(110, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

private void rdoOptionBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionBItemStateChanged
   
}//GEN-LAST:event_rdoOptionBItemStateChanged

private void rdoOptionDItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionDItemStateChanged
    
}//GEN-LAST:event_rdoOptionDItemStateChanged

private void rdoOptionAItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionAItemStateChanged
    
}//GEN-LAST:event_rdoOptionAItemStateChanged

private void rdoOptionCItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionCItemStateChanged
    
}//GEN-LAST:event_rdoOptionCItemStateChanged

    private void lblQuestionAsImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblQuestionAsImageMouseClicked
        //JOptionPane.showMessageDialog(null, "Hello");
         switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                            
                new ZoomImage(qImagePath).setVisible(true);
            }
        }
    }//GEN-LAST:event_lblQuestionAsImageMouseClicked

    private void lblOptionAsImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOptionAsImageMouseClicked
        // TODO add your handling code here:
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                 
                new ZoomImage(oImagePath).setVisible(true);

                System.out.println(oImagePath);
            }
        }
    }//GEN-LAST:event_lblOptionAsImageMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblA;
    private javax.swing.JLabel lblB;
    private javax.swing.JLabel lblC;
    private javax.swing.JLabel lblCorrect;
    private javax.swing.JLabel lblCorrectAnswer;
    private javax.swing.JLabel lblCorrectNumericalAnswer;
    private javax.swing.JLabel lblD;
    private javax.swing.JLabel lblHint;
    private javax.swing.JLabel lblHintImage;
    private javax.swing.JLabel lblHintm;
    private javax.swing.JLabel lblOptionA;
    private javax.swing.JLabel lblOptionAsImage;
    private javax.swing.JLabel lblOptionB;
    private javax.swing.JLabel lblOptionC;
    private javax.swing.JLabel lblOptionD;
    private javax.swing.JLabel lblQuestion;
    private javax.swing.JLabel lblQuestionAsImage;
    private javax.swing.JLabel lblQuestionNo;
    private javax.swing.JLabel lblQuestionNo5;
    private javax.swing.JLabel lblQuestionNo6;
    private javax.swing.JLabel lblRightWrongSymbol;
    private javax.swing.JLabel lblUserNumericalAnswer;
    private javax.swing.JRadioButton rdoOptionA;
    private javax.swing.JRadioButton rdoOptionB;
    private javax.swing.JRadioButton rdoOptionC;
    private javax.swing.JRadioButton rdoOptionD;
    private javax.swing.JLabel txtNumAns;
    private javax.swing.JLabel txtNumericalAnswer;
    // End of variables declaration//GEN-END:variables
}