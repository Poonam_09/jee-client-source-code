/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package registration;

/**
 *
 * @author Rana
 */
public class CUIDGeneratorNew {
    public static String uniqueCPUID()
    {
        String constantkey=new PINGenerator().staticEncrypt(MiscUtils.getMotherboardSN());
        String varkey=new PINGenerator().EncryptTwoString(DiskUtils.getSerialNumber("C"),MiscUtils.getMotherboardSN());
        
        String serialNumber = ""
    + varkey.charAt(32)
    + varkey.charAt(76)
    + varkey.charAt(100)
    + constantkey.charAt(50)
    + varkey.charAt(31)
    + "-"
    + varkey.charAt(2)
    + varkey.charAt(91)
    + constantkey.charAt(73)
    + varkey.charAt(72)
    + varkey.charAt(98)
    + "-"
    + constantkey.charAt(47)
    + varkey.charAt(65)
    + constantkey.charAt(18)
    + varkey.charAt(85)
    + varkey.charAt(7)
    + "-"
    + varkey.charAt(27)
    + varkey.charAt(53)
    + varkey.charAt(102)
    + constantkey.charAt(15)
    + varkey.charAt(99); 
    serialNumber=serialNumber.toUpperCase();
    return serialNumber;
    }
    
}
